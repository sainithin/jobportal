var express = require('express');
var router = express.Router();
flash = require("connect-flash");
const path = require('path'),
  mongoose = require("mongoose"),
  session = require("express-session"),
  moment = require("moment"),
  passport = require("passport"),
   nodemailer = require("nodemailer"),
      async      = require("async"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override");
  const middleware = require("../middleware");
// import models

  const User = require("../models/user");
  const Employer = require("../models/employer");
  const Admin = require("../models/admin");
  const Newjobpost = require("../models/newjob");



  // post new job
  router.get('/:id/newjob',middleware.checkemployerAuthentication,  function(req, res, next) {
        Newjobpost.findById(req.params.id, function(err, users) {
          if(err){
              res.render("employer/employer_dashboard");
          }
          res.render("employer/newjob", {
              job: users
          });
      });

  });

  router.post("/:id/newjob", middleware.checkemployerAuthentication, (req, res) => {
      var jobtitle = req.body.jobtitle;
      var category = req.body.category;
      var joblocation =  req.body.joblocation;
      var jobtype = req.body.jobtype;
      var experience = req.body.experience;
      var jobdate = req.body.jobdate;
      var salaryrange = req.body.salaryrange;
      var qualification = req.body.qualification;
      var gender = req.body.gender;
      var vacancy =req.body.vacancy;
      var jobdescription = req.body.jobdescription;
      var responsibilities = req.body.responsibilities;
      var education = req.body.education;
      var otherbenefits = req.body.otherbenefits;
      var country = req.body.country;
      var city = req.body.city;
      var zipcode = req.body.zipcode;
      var address = req.body.address;
      var companyname = req.body.companyname;
      var website = req.body.website;
      var companyprofile = req.body.companyprofile;
      var package = req.body.package;
      var postedby = req.body.postedby;
      var id = req.params.id;

      var newJob = { jobtitle: jobtitle, category: category,joblocation: joblocation,jobtype: jobtype, experience:experience,salaryrange:salaryrange,qualification: qualification,gender: gender,vacancy: vacancy, jobdescription: jobdescription, responsibilities: responsibilities, education: education, otherbenefits: otherbenefits,  country: country,city: city, zipcode: zipcode, address: address, companyname:companyname, website: website, companyprofile: companyprofile, package: package,postedby:postedby }
      Newjobpost.create(newJob, function (err, newlyCreated) {
          if (err) {
              console.log(err);
          } else {
              req.flash("success", newJob.job + " is successfully added");
              console.log("done");
              console.log(newJob);
              res.redirect("/"+id+"/newjob");
          }
      })
  });
// approve Jobs
  router.post("/:id/accept",middleware.checkadminAuthentication, function(req, res, next){
  Newjobpost.findByIdAndUpdate(req.params.id).exec(function (err, jobs) {
       if (err) {
           console.log(err);
       } else {
           jobs.Permission = 'true';
           jobs.Status = 'true';
           jobs.save();
           res.redirect("/admin_dashboard");
       }
   })
});

  router.get("/:id/jobdetails",function(req,res,next){
  Newjobpost.findById(req.params.id, function(err, users) {
    if(err){
        res.render("user/home");
        console.log("error to load page");
    }
    res.render("user/jobdetails", {
        job: users
    });
});

});


   router.post("/:id/save", function(req, res, next){
    id=req.body.jobid;

      User.findOneAndUpdate(
   { _id: req.params.id },
   {$push: {bookmarked: { jobid:req.body.jobid}}},
  function (error, success) {
        if (error) {
             res.redirect('/'+id+'/user_editresume');
            console.log(error);
        } else {
             res.redirect('/'+id+'/jobdetails');
            console.log(success);
        }
    });
});

  router.get("/:id/aboutus",function(req,res,next){
    Newjobpost.findById(req.params.id, function(err, users) {
      if(err){
          res.render("user/home");
          console.log("error to load page");
      }
      res.render("user/aboutus", {
          job: users
      });
    });

    });

    router.get("/:id/howitworks",function(req,res,next){
    Newjobpost.findById(req.params.id, function(err, users) {
      if(err){
          res.render("user/home");
          console.log("error to load page");
      }
      res.render("user/howitworks", {
          job: users
      });
    });

    });

  router.get("/:id/faq",function(req,res,next){
    Newjobpost.findById(req.params.id, function(err, users) {
      if(err){
          res.render("user/home");
          console.log("error to load page");
      }
      res.render("user/faq", {
          job: users
      });
    });

    });

  router.get("/:id/contactus",function(req,res,next){
      Newjobpost.findById(req.params.id, function(err, users) {
        if(err){
            res.render("user/home");
            console.log("error to load page");
        }
        res.render("user/contact", {
            job: users
        });
      });

      });



  router.delete("/:id/delete",middleware.checkemployerAuthentication, (req, res) => {
      Newjobpost.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
          res.redirect("employer/employer_dashboard");
          console.log(err);
        } else {
              log.console("done deleted");
              res.redirect("employer/managejobs");
        }
    });


});

  // router.delete("/:id/applyjob",middleware.checkuserAuthentication,(req, res) => {
  //
  // } );

module.exports = router;
