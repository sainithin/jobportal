const mongoose = require("mongoose"),
   passportLocalMongoose = require("passport-local-mongoose");
const EmployerSchema = new mongoose.Schema({
    username:String,
    firstname:String,
    email:{type:String},
    phone :Number,
    address: String,
    expertise: String,
    aboutme: String,
    category: String,
    status: String,
    salary: Number,
    age: Number,
    location: String,
    experience: String,
    gender: String,
    qualification: String,
});
EmployerSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model("Employer", EmployerSchema);
