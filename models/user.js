const mongoose = require("mongoose"),
   passportLocalMongoose = require("passport-local-mongoose");
const UserSchema = new mongoose.Schema({
    username:String,
    firstname:String,
    email:{type:String},
    phone :Number,
    address: String,
    expertise: String,
    aboutme: String,
    category: String,
    status: String,
    salary: String,
    age: String,
    location: String,
    experience: String,
    gender: String,
    qualification: String,
    EducationBackground:[{
        title :String,
        institute:String,
        period:String,
        description:String
    }],
    bookmarked:[{
            jobid:String
    }]

});
UserSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model("User", UserSchema);
