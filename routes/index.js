var express = require('express');
var router = express.Router();
flash = require("connect-flash");
const path = require('path'),
  mongoose = require("mongoose"),
  session = require("express-session"),
  moment = require("moment"),
  passport = require("passport"),
   nodemailer = require("nodemailer"),
      async      = require("async"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override");
  const middleware = require("../middleware");
// import models

  const User = require("../models/user");
  const Employer = require("../models/employer");
  const Admin = require("../models/admin");
  const Newjobpost = require("../models/newjob");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

// admin
router.get('/admin_dashboard',middleware.checkadminAuthentication,  function(req, res, next) {
  res.render('admin/admin_dashboard');
});

router.get('/:id/updatejobrequest',  function(req, res, next) {
  Newjobpost.find(function (err, users) {
    if(err){
        res.render("/admin");
    }
   res.render("admin/managejob.ejs", {
        jobs: users
    });
});
});

// user landing page
router.get('/home',middleware.checkuserAuthentication,  function(req, res, next) {

  Newjobpost.find(function (err, users) {
    if(err){
        res.render("/");
    }
   res.render("user/home.ejs", {
        jobs: users
    });
});

});

// user_profile
router.get('/:id/user_profile',middleware.checkuserAuthentication,  function(req, res, next) {
      User.findById(req.params.id, function(err, users) {
        if(err){
            res.render("user/home");
        }
        res.render("user/user_profile", {
            user: users
        });
    });

});
// user_dasboard
router.get('/:id/user_dashboard',middleware.checkuserAuthentication,  function(req, res, next) {
      User.findById(req.params.id, function(err, users) {
        if(err){
            res.render("user/home");
        }
        res.render("user/user_dashboard", {
            user: users
        });
    });

});

// user_settings

router.get('/:id/user_setting',middleware.checkuserAuthentication,  function(req, res, next) {
      User.findById(req.params.id, function(err, users) {
        if(err){
            res.render("user/user_dasboard");
        }
        res.render("user/settings", {
            user: users
        });
    });

});
// user_resume
router.get('/:id/user_resume',middleware.checkuserAuthentication,  function(req, res, next) {
      User.findById(req.params.id, function(err, users) {
        if(err){
            res.render("user/home");
        }
        res.render("user/user_resume", {
            user: users
        });
    });

});
// update about me
router.post('/:id/updateaboutme',middleware.checkuserAuthentication, function (req, res) {
      var email = req.body.email;
   User.findByIdAndUpdate(req.params.id).exec(function (err, list) {
        if (err) {
            console.log(err);
            res.redirect('/user_profile');
        } else {
              list.aboutme = req.body.aboutme;
              list.save();
              console.log("hello");
              res.redirect('/home');
        }
    });
 async.waterfall([
    function(token, user, done) {
      // indicate email account and the content of the confirmation letter
     let smtpTransport = nodemailer.createTransport({
        host:"smtp.gmail.com",
        port: 465,
        secure: true,     //smtp.mail.com
        auth: {
             user: "twitsoftdesign@gmail.com", //username
          pass: "Twitsoft@111"  // password
        }
      });
      let mailOptions = {
           from: "twitsoftdesign@gmail.com",
        to: email,
        subject: "HIPO| Profile Update ", // Subject linehttps://i.imgur.com/
        html: '<html>hello</html>',
        };
      smtpTransport.sendMail(mailOptions, err => {
        if (err) throw err;

        console.log("mail sent");
        done(err, "done");
      });
    }
    ], err => {
      if (err) return next(err);
      res.redirect("/account");
    });
  });
// user_editresume
router.get('/:id/user_editresume',middleware.checkuserAuthentication,  function(req, res, next) {
      User.findById(req.params.id, function(err, users) {
        if(err){
            res.render("user/home");
        }
        res.render("user/user_editresume", {
            user: users
        });
    });

});
// user_bookmarked
router.get('/:id/user_bookmarked',middleware.checkuserAuthentication,  function(req, res, next) {
      Newjobpost.find(function(err, users) {
        if(err){
            res.render("user/home");
        }
        res.render("user/user_bookmarked", {
            job: users
        });
    });

});
// user_appliedjob
router.get('/:id/user_applied',middleware.checkuserAuthentication,  function(req, res, next) {
      Newjobpost.find(function (err, users) {
        if(err){
            res.render("user/home");
        }
       res.render("user/user_appliedjob", {
            jobs: users
        });
    });

});
router.get('/:id/jobs',middleware.checkuserAuthentication,  function(req, res, next) {
      Newjobpost.find(function (err, users) {
        if(err){
            res.render("user/home");
        }
       res.render("user/joblist", {
            jobs: users
        });
    });

});

// Employer dashboard
router.get('/employer_dashboard',middleware.checkemployerAuthentication,  function(req, res, next) {
  res.render('employer/employer_dashboard');
});

// employer_editprofile
router.get('/:id/employer_editprofile',middleware.checkemployerAuthentication,  function(req, res, next) {
  res.render('employer/employer_editprofile');
});

// managejobs
  router.get('/:id/managejobs',middleware.checkemployerAuthentication,  function(req, res, next) {
  Newjobpost.find(function (err, users) {
    if(err){
        res.render("employer/employer_dashboard");
    }
   res.render("employer/managejobs", {
        jobs: users
    });
    });
});

// managecandidates
router.get('/:id/managecandidates',middleware.checkemployerAuthentication,  function(req, res, next) {
  res.render('employer/managecandidates');
});

// shortlisted
router.get('/:id/shortlisted',middleware.checkemployerAuthentication,  function(req, res, next) {
  res.render('employer/shortlisted');
});


module.exports = router;
