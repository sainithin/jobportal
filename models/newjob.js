const mongoose = require("mongoose");
const NewjobpostSchema = new mongoose.Schema({
  jobtitle:String,
  category: String,
  joblocation: String,
  jobtype:String,
  experience:String,
  salaryrange: String,
  jobdate: String,
  qualification: String,
  gender: String,
  vacancy: String,
  jobdescription: String,
  responsibilities: String,
  education: String,
  otherbenefits: String,
  country: String,
  city: String,
  zipcode: Number,
  address: String,
  companyname: String,
  website: String,
  companyprofile: String,
  package: String,
  postedby:String,
  Status: { type: Boolean, default: false },
  Permission: { type: Boolean, default: false }
});
module.exports = mongoose.model("Newjobpost", NewjobpostSchema);
