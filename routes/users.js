var express = require('express');
var router = express.Router();
flash = require("connect-flash");
const path = require('path'),
  mongoose = require("mongoose"),
  session = require("express-session"),
  moment = require("moment"),
  passport = require("passport"),
   nodemailer = require("nodemailer"),
      async      = require("async"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override");
  const middleware = require("../middleware");
// import models
  const User = require("../models/user");
  const Employer = require("../models/employer");
  const Admin = require("../models/admin");
  const Newjobpost = require("../models/newjob");


// admin
router.get('/admin_register', function(req, res, next) {
  res.render('register/admin_signup');
});
// admin register
router.post("/admin_register", (req, res) => {
    let newUser = new Admin({
        username: req.body.username,
        email: req.body.email
    });

    if (req.body.admincode == 1463) {
        newUser.Admin = true;
    }
    else {
        req.flash("error", "Entered Wrong code contact Administrator");
        return res.redirect("/login/admin_login");
    }
    Admin.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                console.log("error", "That email has already been registered.");
                return res.redirect("/login/admin_login");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            return res.redirect("/logi/admin_login");
        }

        passport.authenticate("admin")(req, res, () => {
            req.flash("success", "Welcome  " + user.username);
            console.log("welcome login");
            res.redirect("/admin");
        });
    });
});
//  admin login
router.get('/admin', function(req, res, next) {
  res.render('login/admin_login');
});
router.post("/admin_login", (req, res, next) => {
    passport.authenticate("admin", (err, user, info) => {
        if (err) {
            return next(err);
        }
        if (!user) {
            req.flash("error", "Invalid username or password");
            console.log(err);
            return res.redirect('/admin');
        }
        req.logIn(user, err => {
            if (err) {
                return next(err);
            }
            let redirectTo = req.session.redirectTo ? req.session.redirectTo : ('/admin_dashboard');
            delete req.session.redirectTo;
            req.flash("success", "Good to see you again, " + user.username);
            console.log("login admin");
            res.redirect(redirectTo);
        });
    })(req, res, next);
});


/* Employer register */
router.get('/employer_signup', function(req, res, next) {
  res.render('register/employer_signup');
});
router.get('/employer_signin', function(req, res, next) {
  res.render('login/employer_signin');

});
router.post("/employer_signup", (req, res) => {
    let newUser = new Employer({
        username: req.body.username,
        firstname: req.body.firstname,
        email:req.body.email,
    });
    Employer.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                console.log(err);
                return res.redirect("/employer");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            console.log("Something wrong");
            return res.redirect("/employer");
        }

        passport.authenticate("emp")(req, res, () => {
            req.flash("success", "Welcome  " + user.username);
            console.log(newUser);
            res.redirect("/employer_signin");
        });
    });
});
//  employer login
router.post("/employer_signin", (req, res, next) => {
    passport.authenticate("emp", (err, user, info) => {
        if (err) {
            return next(err);
        }
        if (!user) {
            req.flash("error", "Invalid username or password");
            return res.redirect('/employer_signin');
        }
        req.logIn(user, err => {
            if (err) {
                return next(err);
            }
            let redirectTo = req.session.redirectTo ? req.session.redirectTo : ('/employer_dashboard');
            delete req.session.redirectTo;
            req.flash("success", "Good to see you again, " + user.username);
            res.redirect(redirectTo);
        });
    })(req, res, next);
});




/* User register */
router.get('/signup', function(req, res, next) {
  res.render('register/user_signup');
});

router.post("/user_signup", (req, res) => {
    let newUser = new User({
        username: req.body.username,
        firstname: req.body.firstname,
        email:req.body.email,
        phone:req.body.phone,
        address:req.body.address,
        expertise:req.body.expertise,
        aboutme:req.body.aboutme,

    });
    User.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                console.log(err);
                return res.redirect("/user_signup");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            console.log("Something wrong");
            return res.redirect("/employer");
        }

        passport.authenticate("user")(req, res, () => {
            req.flash("success", "Welcome  " + user.username);
            res.redirect("/");
        });
    });
});

router.post("/user_signin", (req, res, next) => {
    passport.authenticate("user", (err, user, info) => {
        if (err) {
            return next(err);
        }
        if (!user) {
            req.flash("error", "Invalid username or password");
            return res.redirect('/');
        }
        req.logIn(user, err => {
            if (err) {
                return next(err);
            }
            let redirectTo = req.session.redirectTo ? req.session.redirectTo : ('/home');
            delete req.session.redirectTo;
            req.flash("success", "Good to see you again, " + user.username);

            res.redirect(redirectTo);
            req.send(req.flash('success'));
        });
    })(req, res, next);
});

// user_editresume

router.post("/:id/updateaboutme_resume",middleware.checkuserAuthentication, function (req, res) {

var id = req.params.id;
   User.findByIdAndUpdate(req.params.id, {upsert:true}, {new : true, returnNewDocument: true}).exec(function (err, list) {
        if (err) {
            console.log(err);
            res.redirect('/user_profile');
        } else {
              list.aboutme = req.body.aboutme;
               list.category = req.body.category;
                list.location = req.body.location;
               list.status = req.body.status;
               list.experience = req.body.experience;
                 list.salary = req.body.salary;
                list.age = req.body.age;
               list.gender = req.body.gender;
               list.qualification = req.body.qualification;
              list.save();
              console.log("hello");
              res.redirect('/'+id+'/user_editresume');
        }
    });


});


router.post("/:id/updateeducation_background",middleware.checkuserAuthentication, function (req, res) {

var id = req.params.id;
   // User.findByIdAndUpdate(req.params.id, {upsert:true}, {new : true, returnNewDocument: true}).exec(function (err, list) {
   //      if (err) {
   //          console.log(err);
   //          res.redirect('/user_profile');
   //      } else {
   //            list.aboutme = req.body.aboutme;
   //             list.qualification = req.body.qualification;
   //            list.save();
   //            res.redirect('/'+id+'/user_editresume');
   //      }
   //  });

   // User.findOneAndUpdate({id: req.params.id}, {$push: {EducationBackground: { title:req.body.title,institute:req.body.institute,period:req.body.period,description:req.body.description}}},{upsert:true}, {new : true, returnNewDocument: true})

   //            res.redirect('/'+id+'/user_editresume');


    User.findOneAndUpdate(
   { _id: req.params.id },
   {$push: {EducationBackground: { title:req.body.title,institute:req.body.institute,period:req.body.period,description:req.body.description}}},
  function (error, success) {
        if (error) {
             res.redirect('/'+id+'/user_editresume');
            console.log(error);
        } else {
             res.redirect('/'+id+'/user_editresume');
            console.log(success);
        }
    });


});


// logout
router.get("/logout", (req, res) => {
    req.logout();
    req.flash("success", "Logged out successfully. Looking forward to seeing you again!");
    res.redirect("/");
});
module.exports = router;
