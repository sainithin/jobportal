const mongoose = require("mongoose"),
   passportLocalMongoose = require("passport-local-mongoose");
const AdminSchema = new mongoose.Schema({
    username:String,
    email:String,

});
AdminSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model("Admin", AdminSchema);
